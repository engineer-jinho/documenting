# 문서화

* 파이썬 기술문서화
  - sphinx
  - epydoc
  - docstring
  - pdoc
  - pydoctor
  - doxygen
  - mkdocs

* mkdocs 주요 테마
  - mkdocs material

* sphinx 주요 테마
  - book theme
    - https://sphinx-book-theme.readthedocs.io/en/latest/index.html
    - https://jupyterbook.org/en/stable/intro.html
    - https://inferentialthinking.com/
    - 



## 위키

* WikiDocs
  - https://github.com/Zavy86/WikiDocs
* Zim Wiki
* Doku wiki
